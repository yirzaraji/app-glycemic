fetch('../model/aliments.json',
    {
        method: "POST",
        dataType: "json"
    })
.then(response => response.json())

.then(data => {
    console.log(data)
    //starting to loop over data
    for(let y = 0; y < data.length; y++){
   
        console.log(y)
        let scontain = document.getElementById("scontain")

        //create a div with some attributes for each loop element
        let newDiv = document.createElement("div");
        newDiv.className = "alime mt-3 mr-2 ml-2 p-2"
        newDiv.setAttribute("width", "120px");
        newDiv.setAttribute("height", "120px");

        //insert div into the dom as child of scontain
        scontain.appendChild(newDiv);

        let imageItem = document.createElement("img");
        imageItem .setAttribute("src", data[y].urlavatar);
        imageItem.setAttribute("width", "50px");
 
        let brElement = document.createElement("br");

        let texteNode = document.createElement("span");
        texteNode.className = "spantext"

        //insert image item dans la div generée dynamiquement
        newDiv.appendChild(imageItem);
        newDiv.appendChild(brElement);
        newDiv.appendChild(texteNode);
               
        let spantarg = document.querySelectorAll('.spantext')
        spantarg[y].innerHTML = data[y].nom

        //appendDivItem is a fonction that generate div element (line 19)
        let alime = document.querySelectorAll('.alime')
        alime[y].addEventListener("click",  function(){

            let icontain = document.querySelector('#icontain')
            let divAdd = document.createElement("div");
            divAdd.id = "item" + y
            divAdd.className = "mt-3 mr-2 ml-2 p-2 itemAdded " + y
            icontain.appendChild(divAdd);
    
            let imageItem = document.createElement("img");
            imageItem.setAttribute("src", data[y].urlavatar);
            imageItem.setAttribute("width", "50px");
            divAdd.appendChild(imageItem);
        
            let brElement = document.createElement("br");
            divAdd.appendChild(brElement);
            
            let texteNode = document.createElement("span");
            texteNode.className = "spantextBis"
            divAdd.appendChild(texteNode);
            
            //inner into span
            texteNode.innerHTML = data[y].nom 
            
            //remove div itemAdded if clicked
            let divad = document.querySelectorAll('.itemAdded')
            for(let y = 0; y<divad.length; y++){
                divad[y].addEventListener("click",  function(){
                    divad[y].remove();                    
                })
            }

            let calculButton = document.getElementById('calculButton')
            calculButton.addEventListener("click",  function(){

                //starting to calcul SUM of selected Food by User
                let sumCG = 0
                let sumNT = 0
                let sumFood = ' '
                let sumGL = 0
                for (let z = 0; z < divad.length; z++) {
                    sumCG += data[y].cg
                    sumNT += data[y].nutriscore
                    sumGL += data[y].glucide
                    sumFood += data[y].nom + ' '
                }

                console.log(sumCG + ' ' + sumGL + ' '  + sumNT + ' ' + sumFood)

                //MESSAGE TO INNER
                let titleMessage = document.getElementById('titleMessage')
                let messageCorps = document.getElementById('messageCorps')

                //DIV TO INNER
                let CGnumber = document.getElementById('CGnumber')
                let GLnumber = document.getElementById('GLnumber')
                let NTnumber = document.getElementById('NTnumber')

                CGnumber.innerHTML = sumCG + '<span class="petitNumber">Cg</span>'
                GLnumber.innerHTML = sumGL + '<span class="petitNumber">Gl</span>'
                NTnumber.innerHTML = sumNT + '<span class="petitNumber">Nt</span>'
        
                if(sumCG <= 10){
                    titleMessage.innerHTML = "<b>REPAS PARFAITEMENT EQUILIBRE</b>";
                    messageCorps.innerHTML = "Bravo votre charge glycemique est tres faible votre repas est tres equilibré";
                }
                else if(sumCG <= 20){
                    titleMessage.innerHTML = "<b>REPAS TROP LOURD</b>";
                    messageCorps.innerHTML = "Votre charge glycemique depasse 10 nous vous conseillons de prendre RDV avec un specialiste";
                }
                else if(sumCG <= 60){
                    titleMessage.innerHTML = "<b>CAS DE MORT IMMINENTE</b>";
                    messageCorps.innerHTML = "Votre charge glycemique depasse 20 vous avez 90% de chance de mourir a 50 ans";
                }
        
                let saveButton = document.getElementById('saveButton')
                saveButton.style.display = 'block'    
                
                //Ajax request to send js variables to the server
                function saveRepas(){
                    $.ajax({
                        type: 'post', // the method
                        url: '../controller/saveRepas.php', // The file where my php code is
                        data: {
                            'calculSave': sumCG, // all variables i want to pass.
                            'nomAliments': sumFood,
                            'calculGl': sumGL,
                            'calculNT': sumNT
                        },
                        success: function(data) { // in case of success get the output, i named data
                            //alert('Votre repas a ete sauvegardé en base de données !')
                            saveButton.innerHTML = "<b>Repas Saved</b>";
                            saveButton.style.background = '#00b990'
                        },
                        error: function(data){
                            console.log(data)
                        }
                    });
                }
                saveButton.addEventListener("click",  function(){
                //console.log(calculCG)
                saveRepas()
                })
            })
        })//END EVENT LISTENER        
    }//END LOOP
})

.catch(error => console.error(error));


<?php
session_start();
error_reporting(-1);
ini_set('display_errors', 'On');

include ('../model/db.php');
connectdb();

if(isset($_POST['calculSave']) && isset($_POST['nomAliments'])) { 
  
    $calculSave = htmlspecialchars($_POST['calculSave']);
    $composition_repas = htmlspecialchars($_POST['nomAliments']);
    $nutriscore = htmlspecialchars($_POST['calculNT']);
    $glucide = htmlspecialchars($_POST['calculGl']);


    $req = $db->prepare('INSERT INTO repas_user (composition_repas, cg, nutriscore, glucide) VALUES(?, ?, ?, ?)');
    $req->execute(array($composition_repas, $calculSave, $nutriscore, $glucide));
    //do some sql request
}
else{
    echo "Error: Something bad happened";
}
<?php
session_start();
error_reporting(-1);
ini_set('display_errors', 'On');

include ('../model/db.php');
connectdb();

//get user logs from connexion form
$_SESSION['login'] = htmlspecialchars($_POST['login']);
$_SESSION['mail'] = htmlspecialchars($_POST['mail']);
$_SESSION['password'] = htmlspecialchars($_POST['password']);


//sql request for select needed datas
$req = $db->prepare('SELECT * FROM users WHERE (login, mail) = (:login, :mail)');
$req->execute(array(

    'login' => $_SESSION['login'],
    'mail' => $_SESSION['mail']));
    

//fetch is a method for return datas in an array
$resultat = $req->fetch();
$fetchLogin = $resultat['login'];
$fetchMail = $resultat['mail'];

//var_dump([$resultat]);

//this variable use password_verify method to compare hashed password into the db by the one from form
$isPasswordCorrect = password_verify($_SESSION['password'], $resultat['password']);

//condition that lead users to log again or to access a page that only connected users can access
if (!$isPasswordCorrect){
    $_SESSION['fail'] = true;
    header('Location: ../index.php');
}

else if($isPasswordCorrect && $fetchLogin && $fetchMail)
{  
    $_SESSION['success'] = true;
    header('Location: ../view/memberSpace.php');
}

?>
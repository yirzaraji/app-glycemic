<?php 
    include ('view/header.php');
    include ('model/db.php');
    connectdb();

    

?>

<div class="container-fluid">
    <div class="row justify-content-center">
        <div class="col-md-4 bg-light mt-4">
            <?php include ('view/formRepository/connexionForm.php'); 
                if(isset($_SESSION['fail'])){
                ?>
                <div class="alert alert-danger" role="alert">
                    Wrong password or login, try again !
                </div>
                <?php
                session_destroy();
            }
            ?>
        </div>
    </div>
</div>

<?php include ('view/footer.php');?>
-- phpMyAdmin SQL Dump
-- version 4.9.2
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1:3306
-- Généré le :  Dim 13 sep. 2020 à 15:03
-- Version du serveur :  10.4.10-MariaDB
-- Version de PHP :  7.1.33

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `app_glycemic`
--

-- --------------------------------------------------------

--
-- Structure de la table `repas_user`
--

DROP TABLE IF EXISTS `repas_user`;
CREATE TABLE IF NOT EXISTS `repas_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `composition_repas` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `cg` int(11) NOT NULL,
  `nutriscore` int(11) NOT NULL,
  `glucide` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `repas_user`
--

INSERT INTO `repas_user` (`id`, `composition_repas`, `cg`, `nutriscore`, `glucide`) VALUES
(1, 'repas1', 14, 12, 22),
(2, 'repas1', 14, 12, 22),
(3, 'repas1', 14, 12, 22),
(4, 'Riz steak hache Gratin dauphinois', 14, 20, 8),
(5, 'Riz steak hache Gratin dauphinois', 14, 20, 8);

-- --------------------------------------------------------

--
-- Structure de la table `users`
--

DROP TABLE IF EXISTS `users`;
CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `login` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `mail` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` text COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `users`
--

INSERT INTO `users` (`id`, `login`, `mail`, `password`) VALUES
(1, 'Remi', 'test@gmail.com', '$2y$10$BNTwvaevjtCA5nkiAzyg4O/TxcBw.uGD6LPytVlHI93PQ83zCVNQK');
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

<nav>
    <div class="container bg-danger">
        <div class="row align-items-center navBarColor">
            <div class="col-3 col-sm-1 col-md-1">
                <span id="burgerIcon" class="material-icons">menu</span>
            </div>
            <div class="col-9 col-xs-11 col-sm-11 col-md-11 mt-3 d-flex justify-content-end">
                <button class="btn btn-info" id="profil-btn">Bonjour <?php echo $_SESSION['login']?></button>
                <form action="memberSpace.php" method="post">
                    <div class="form-group">
                        <button class="btn btn-danger" type="submit" name="deco">
                            <i class="fa fa-power-off"></i>
                        </button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</nav>
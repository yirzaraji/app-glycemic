
<?php 
    error_reporting(-1);
    ini_set('display_errors', 'On');
    include ('header.php');
    include ('../model/db.php');
    connectdb();
    

    if(isset($_SESSION['success'])){  
        include ('navBar.php');
        
?>
    <div class="container app">
        <div class="row pt-5">
            <div class="col-md-12 text-center logoBlock">
                <img src="../app/images/logo.png" class="text-center img-responsive w-32"><br>
                <span class="colorAppFontA">GLYCEMIC</span><span class="colorAppFontB">APP</span>
            </div>
        </div>
        <div class="row  pt-5 pb-5">
            <div class="col-md-3"></div>
            <div class="col-md-6 text-center titleCG">
            <p class="mb-3 p-1"><span class="colorAppFontA">CALCUL LA CHARGE<br> GLYCEMIQUE DE TON REPAS !</span></p>
            Grace a notre superbe application tu peux composer
            ton repas en selectionnant tes aliments pour ton entrée ton plat et ton
            dessert.</p>
            <p>Commence par selectionner tes aliments !</p>
            </div>
            <div class="col-md-3"></div>
        </div>
    </div>

    <div class="container app pt-5 pb-5">
        <div class="row justify-content-center">          
            <div class="col-11 col-md-10 col-lg-6 mt-4 text-center">
                <h5><span class="colorAppFontA">COMPOSES TON </span>
                <span class="colorAppFontB">REPAS</span></h5>
                <div class="row text-center boxItemRow pb-3 dynamicDiv justify-content-center" id="scontain">
                <!-- here js generated div   --> 
                </div>
            </div>          
        </div>
    </div>
    <div class="container app pb-5">
    <h2 class="titleSelect text-center"><span class="colorAppFontA">ALIMENTS </span><span class="colorAppFontB">SELECTIONNES</span></h2>
        <div class="row justify-content-center">  
            <div class="col-11 col-md-10 col-lg-6 text-center dynamicDiv" id="selectedAliment">
                <div class="row text-center boxItemRow justify-content-center" id="icontain">
                <!-- here js generated div   -->     
                </div>
            </div>
        </div>
    </div>

    <div class="container pt-5 app">
        <div class="row text-center">
            <div class="col-md-5"></div>
            <div class="col-md-2 mb-5">
            <span class="resultatSpan">RESULTAT</span>
            </div>
            <div class="col-md-5"></div>
        </div>
        <div class="row d-flex justify-content-center text-center align-items-center">
            <div class="col-xs-2 mr-2">
                <div class="bubble smallbubble">
                <span id="GLnumber">GL</span>
                </div>
            </div>
            <div class="col-xs-2 mr-2">
                <div class="bubble">
                    <span id="CGnumber">CG</span>
                </div>
            </div>
            <div class="col-xs-2 mr-2">
                <div class="bubble smallbubble">
                <span id="NTnumber">NT</span>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12 d-flex justify-content-center">
                <button class="btn btn-info mt-3" id="calculButton"><b>Stats Repas</b></button>
            </div>
        </div>
        
        <div class="row justify-content-center text-center message mt-5">
            <div class="col-1 col-sm-4 col-md-4 d-flex align-items-center imgMessage img-responsive">
                <img src="../app/images/meal.svg" class="text-center img-responsive">
            </div>
            <div class="col-8 col-sm-4 col-md-4 messageTr imgMessage">
                <img src="../app/images/alert.svg" class="img-responsive mt-3">
                <p id="titleMessage"><b>MESSAGE</b></p>
                <p id="messageCorps">Details du message</p>
                <p id="messageTel"><b>06 32 09 67 92</b></p>
            </div>
            <div class="col-1 col-sm-4 col-md-4 d-flex align-items-center imgMessage">
                <img src="../app/images/save.svg" class="text-center img-responsive">
            </div>
        </div>
        <div class="row justify-content-center text-center mt-5 pb-5 endApp">
        
        <button class="btn btn-info mb-5" id="saveButton"><b>Sauvegarde ton repas</b></button>
        </div>
    </div>

    <?php
    if(isset($_POST['deco'])){
        session_destroy();
    }
    }else{
        echo "".'</br>';
        ?>
        <div class="container-fluid">
            <div class="row justify-content-center text-center">
                <div class="col-md-4 mb-5 mt-5">
                <h5>Please connect you for see this page</h5>
               <a href="../index.php"><button class="btn btn-info"> Connexion</button></a>
                </div>
            </div>
        </div>       
        <?php     
    }//end Else
?>

<?php
include ('footer.php');
?>

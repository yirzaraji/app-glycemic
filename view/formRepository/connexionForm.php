<form method="post" action="controller/connexion.php">
    <h3 class="text-center">Connexion</h3>
    <div class="form-group">
        <label for="">Login</label>
        <input type="text" class="form-control" 
        placeholder="Indiquez votre pseudo" name="login" 
        required="required">
    </div>
    <div class="form-group">
        <label for="">Email</label>
        <input type="email" class="form-control" 
        placeholder="Indiquez votre email" name="mail"
        required="required">
    </div>
    <div class="form-group">
        <label for="">Password</label>
        <input type="password" class="form-control" 
        placeholder="Indiquez votre password" name="password" 
        required="required">
    </div>
    <button type="submit" class="btn btn-primary" name="subbouton">Envoyer</button>
</form>